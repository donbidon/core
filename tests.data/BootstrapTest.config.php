;<?php die; __halt_compiler();
[core]

; By default: Off
event[debug] = On

; By default: "StdOut"
log[method] = "UTLogMethod"

; By default: E_ERROR | E_WARNING
log[level] = E_ERROR | E_WARNING | E_NOTICE

;log.source[] = "*" ; means all log sources
log.source[] = "LogTest::someMethod()"

; log[path]     = "..." ; For file logger, see donbidon\Lib\FileSystem\Logger
; log[maxSize]  = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
; log[rotation] = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
; log[righs]    = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
