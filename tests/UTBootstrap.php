<?php
/**
 * Bootstrap class extension for unit testing.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core;

use donbidon\Core\Registry\UTRecursiveRegistry;

/**
 * Bootstrap class extension for unit testing.
 */
class UTBootstrap extends Bootstrap
{
    /**
     * {@inheritDoc}
     *
     * @param  array $config
     * @param  int   $options
     * @return UTRecursiveRegistry
     */
    protected static function getRegistry(array $config, $options)
    {
        UTRecursiveRegistry::resetInstance();
        $result = UTRecursiveRegistry::getInstance($config, $options);

        return $result;
    }
}
