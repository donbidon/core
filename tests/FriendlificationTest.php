<?php
/**
 * Friendlification class unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core;

/**
 * Friendlification class unit tests.
 */
class FriendlificationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Const for testing
     *
     * @see self::testGetConstNameByValue()
     */
    const TEST_ONE = 0x1001;

    /**
     * Const for testing
     *
     * @see self::testGetConstNameByValue()
     */
    const TEST_TWO = 0x1002;

    /**
     * Tests getting class consts names by its values.
     *
     * @return void
     * @covers donbidon\Core\Friendlification::getConstNameByValue
     */
    public function testGetConstNameByValue()
    {
        $this->assertEquals(
            'TEST_ONE',
            Friendlification::getConstNameByValue(__CLASS__, 0x1001)
        );
        $this->assertEquals(
            'TEST_TWO',
            Friendlification::getConstNameByValue(__CLASS__, 0x1002)
        );
        $this->assertEquals(
            FALSE,
            Friendlification::getConstNameByValue(__CLASS__, 0x1003)
        );
    }
}
