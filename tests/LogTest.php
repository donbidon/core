<?php
/**
 * Logger classes unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Log;

use donbidon\Core\Registry\UTRecursiveRegistry;
use donbidon\Core\Log\T_Logger;
use donbidon\Core\Event\Manager;

/**
 * Logger classes unit tests.
 */
class LogTest extends \donbidon\Lib\PHPUnit\TestCase
{
    use T_Logger;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        UTRecursiveRegistry::resetInstance();
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();

        UTRecursiveRegistry::resetInstance();
    }

    /**
     * Tests invalid logging method.
     *
     * @return void
     * @covers donbidon\Core\Log\Factory::run
     */
    public function testInvalidMethod()
    {
        $this->expectException(\Error::class);
        $this->expectExceptionMessage(
            "Class '\\donbidon\\Core\\Log\\Method\\Invalid' not found"
        );

        $registry = new UTRecursiveRegistry([
            'core' => [
                'log' => [
                    'method' => 'Invalid',
                ],
            ],
        ]);
        $evtManager = new Manager;
        Factory::run($registry, $evtManager);
    }

    /**
     * Tests functionality.
     *
     * @return void
     * @covers donbidon\Core\Log\T_Logger::log
     */
    /*
    public function testFunctionality()
    {
        UTRecursiveRegistry::resetInstance();
        $registry = \donbidon\Core\UTBootstrap::initByPath(
            "../tests.data/BootstrapTest.config.php"
        );
        $this->evtManager = $registry->get('core/event/manager');

        $this->someMethod();
        $this->otherMethod();
        UTRecursiveRegistry::_delete('core/log/level');
        $this->someMethod();
        $source = UTRecursiveRegistry::_get('core/log/source');
        $source[] = "*";
        UTRecursiveRegistry::_set('core/log/source', $source);
        $this->otherMethod();

        $expected = [
            [
                'message' => 'Notice',
                'source'  => 'LogTest::someMethod()',
                'level'   => E_NOTICE,
            ],
            [
                'message' => 'Warning',
                'source'  => 'LogTest::someMethod()',
                'level'   => E_WARNING,
            ],
            [
                'message' => 'Error',
                'source'  => 'LogTest::someMethod()',
                'level'   => E_ERROR,
            ],
            [
                'message' => 'Warning',
                'source'  => 'LogTest::someMethod()',
                'level'   => E_WARNING,
            ],
            [
                'message' => 'Error',
                'source'  => 'LogTest::someMethod()',
                'level'   => E_ERROR,
            ],
            [
                'message' => 'Warinig from other method',
                'source'  => 'LogTest::otherMethod()',
                'level'   => E_WARNING,
            ],
        ];
        $this->assertEquals($expected, UTRecursiveRegistry::_get('log/unittest'));
    }
    */

    /**
     * Tests file logger.
     *
     * @return void
     * @covers donbidon\Core\Log\Method\File::__construct
     * @covers donbidon\Core\Log\Method\File::log
     * @covers donbidon\Core\Log\T_Logger::log
     */
    public function testFileLogger()
    {
        $registry = \donbidon\Core\UTBootstrapFileLogger::initByPath(
            "../tests.data/BootstrapTest.config.php"
        );
        $this->evtManager = $registry->get('core/event/manager');

        $this->someMethod();
        $log = file_get_contents($registry->get('core/log/path'));
        $actual = preg_replace("/\[ \d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} \] /s", "", $log);
        $expected = implode(PHP_EOL, [
            "[ note ] [ LogTest::someMethod() ] ~ Notice",
            "[ WARN ] [ LogTest::someMethod() ] ~ Warning",
            "[ ERR  ] [ LogTest::someMethod() ] ~ Error",
            "",
        ]);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Method used for testing logging functionality.
     *
     * @returns void
     * @see     self::testFunctionality()
     * @see     self::testFileLogger()
     * @internal
     */
    protected function someMethod()
    {
        $this->log("Notice",  'LogTest::someMethod()', E_NOTICE);
        $this->log("Warning", 'LogTest::someMethod()', E_WARNING);
        $this->log("Error",   'LogTest::someMethod()', E_ERROR);
    }

    /**
     * Method used for testing logging functionality.
     *
     * @returns void
     * @see     self::testFunctionality()
     * @internal
     */
    protected function otherMethod()
    {
        $this->log("Warinig from other method",  'LogTest::otherMethod()', E_WARNING);
    }
}
