<?php
/**
 * Registry class unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Event;

use InvalidArgumentException;
use RuntimeException;
use donbidon\Core\Registry\I_Registry;

/**
 * Registry class unit tests.
 */
class EventsTest extends \donbidon\Lib\PHPUnit\TestCase
{
    /**
     * Event manager instance
     *
     * @var Manager
     */
    protected $evtManager;

    /**
     * Debug event results
     *
     * @var array
     */
    protected $debugEventsResults = [];

    /**
     * Fired event uid
     *
     * @var string
     */
    protected $uid;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $this->evtManager = new Manager;
    }

    /**
     * Tests args functionality.
     *
     * @return void
     * @covers donbidon\Core\Event\Args::__construct
     * @covers donbidon\Core\Event\Args::set
     * @covers donbidon\Core\Event\Args::exists
     * @covers donbidon\Core\Event\Args::isEmpty
     * @covers donbidon\Core\Event\Args::get
     * @covers donbidon\Core\Event\Args::delete
     */
    public function testArgs()
    {
        $scope = ['arg' => 'value', 'null' => null, 'empty' => FALSE];
        $args = new Args($scope);

        $this->assertEquals($scope, $args->get());
        $this->assertEquals('value', $args->get('arg'));
        $this->assertEquals('value', $args->get('arg', 'otherValue'));
        $this->assertEquals(null, $args->get('null', 'otherValue'));
        $this->assertEquals(FALSE, $args->get('empty', 'otherValue'));
        $this->assertEquals('missingValue', $args->get('missingArg', 'missingValue'));

        $this->assertFalse($args->isEmpty('arg'));
        $this->assertTrue($args->isEmpty('null'));
        $this->assertTrue($args->isEmpty('empty'));
        $this->assertTrue($args->isEmpty('missingArg'));

        $this->assertTrue($args->exists('arg'));
        $this->assertTrue($args->exists('null'));
        $this->assertTrue($args->exists('empty'));
        $this->assertFalse($args->exists('missingArg'));

        $args->delete('arg');
        $this->assertEquals('missingValue', $args->get('arg', 'missingValue'));
        $this->assertTrue($args->isEmpty('arg'));
        $this->assertFalse($args->exists('arg'));

        $args->set('newArg', 'newValue');
        $this->assertFalse($args->isEmpty('newArg'));
        $this->assertTrue($args->exists('newArg'));
    }

    /**
     * Tests exception when adding handler.
     *
     * @return void
     * @covers donbidon\Core\Event\Manager::addHandler
     */
    public function testExceptionWhenAddingHandler()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid event priority");
        $this->evtManager->addHandler('event', 'foo', -1);
    }

    /**
     * Tests exception when dropping debug event.
     *
     * @return void
     * @covers donbidon\Core\Event\Manager::dropHandlers
     */
    public function testExceptionWhenDroppingDebugEvent()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Cannot drop debug event");
        $this->evtManager->dropHandlers(':log:');
    }

    /**
     * Tests exception when disabling debug event.
     *
     * @return void
     * @covers donbidon\Core\Event\Manager::disableHandler
     */
    public function testExceptionWhenDisablingDebugEvent()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Cannot disable debug event");
        $this->evtManager->disableHandler(':log:');
    }

    /**
     * Tests exception when invalid handler passed.
     *
     * @return void
     * @covers donbidon\Core\Event\Manager::addHandler
     * @covers donbidon\Core\Event\Manager::fire
     */
    public function testExceptionWhenInvalidHadlerPassed()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage(
            "Invalid event handler function someUnexistentFunction() added to process 'event' event"
        );
        $this->evtManager->addHandler('event', 'someUnexistentFunction');
        $this->evtManager->fire('event', new Args);
    }

    /**
     * Tests exception when event fired already.
     *
     * @return void
     * @covers donbidon\Core\Event\Manager::addHandler
     * @covers donbidon\Core\Event\Manager::fire
     */
    public function testExceptionWhenEventFiredAlready()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("Event 'event' is fired already");
        $this->evtManager->addHandler('event', [$this, 'onFiringSameEvent']);
        $this->evtManager->fire('event', new Args);
    }

    /**
     * Hadler firing already fired event.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testExceptionWhenEventFiredAlready()
     */
    public function onFiringSameEvent($name, I_Registry $args)
    {
        $this->evtManager->fire($name, $args);
    }

    /**
     * Tests event hadling.
     *
     * @return void
     * @covers donbidon\Core\Event\Manager::addHandler
     * @covers donbidon\Core\Event\Manager::fire
     */
    public function testEventHandling()
    {
        $this->evtManager->addHandler('first', [$this, 'onB']);
        $this->evtManager->addHandler(
            'first', [$this, 'onLowPriorityB'], Manager::PRIORITY_LOW
        );
        $this->evtManager->addHandler(
            'first', [$this, 'onLowPriorityA'], Manager::PRIORITY_LOW
        );
        $this->evtManager->addHandler('first', [$this, 'onA']);
        $this->evtManager->addHandler(
            'first', [$this, 'onHighPriority'], Manager::PRIORITY_HIGH
        );
        $this->evtManager->addHandler('second', [$this, 'onA']);
        $this->evtManager->addHandler('second', [$this, 'onBreakingEvent']);
        $this->evtManager->addHandler('second', [$this, 'onB']);

        $args = new Args;
        $this->evtManager->fire('first', $args);
        $expected = [
            'donbidon\Core\Event\EventsTest::onHighPriority' => TRUE,
            'donbidon\Core\Event\EventsTest::onB'            => TRUE,
            'donbidon\Core\Event\EventsTest::onA'            => TRUE,
            'donbidon\Core\Event\EventsTest::onLowPriorityB' => TRUE,
            'donbidon\Core\Event\EventsTest::onLowPriorityA' => TRUE,
        ];
        $this->assertEquals($expected, $args->get());

        $args = new Args;
        $this->evtManager->fire('second', $args);
        $expected = [
            'donbidon\Core\Event\EventsTest::onA' => TRUE,
            ':break:'                             => TRUE,
        ];
        $this->assertEquals($expected, $args->get());
    }

    /**
     * Tests debugging events.
     *
     * @return void
     * @covers donbidon\Core\Event\Manager::addHandler
     * @covers donbidon\Core\Event\Manager::fire
     * @covers donbidon\Core\Event\Manager::disableHandler
     * @covers donbidon\Core\Event\Manager::enableHandler
     * @covers donbidon\Core\Event\Manager::dropHandlers
     */
    public function testDebuggingEvents()
    {
        $this->evtManager->addHandler(':onAddHandler:', [$this, 'onAddHandler']);
        $this->evtManager->addHandler(':onEventStart:', [$this, 'onEventStart']);
        $this->evtManager->addHandler(':onHandlerFound:', [$this, 'onHandlerFound']);
        $this->evtManager->addHandler(':onEventEnd:', [$this, 'onEventEnd']);
        $this->evtManager->addHandler(':onDisableHandler:', [$this, 'onDisableHandler']);
        $this->evtManager->addHandler(':onEnableHandler:', [$this, 'onEnableHandler']);
        $this->evtManager->addHandler(':onDropHandlers:', [$this, 'onDropHandlers']);
        $this->evtManager->setDebug(TRUE);

        $this->evtManager->addHandler('first', [$this, 'onA']);
        $this->evtManager->addHandler('first', [$this, 'onA']);
        if (empty($this->debugEventsResults['onAddHandler'])) {
            $this->assertNotEmpty($this->debugEventsResults['onAddHandler']);
        } else {
            $expected = [
                0 => [
                    'name'    => 'first',
                    'handler' => [$this, 'onA'],
                    'added'   => TRUE,
                    'source'  => 'core:event:debug',
                    'level'   => E_NOTICE,
                ],
                1 => [
                    'name'    => 'first',
                    'handler' => [$this, 'onA'],
                    'added'   => FALSE,
                    'source'  => 'core:event:debug',
                    'level'   => E_WARNING,
                ],
            ];
            $this->assertEquals($expected, $this->debugEventsResults['onAddHandler']);
        }

        $args = new Args;
        $this->evtManager->fire('first', $args);
        if (empty($this->debugEventsResults['onEventStart'])) {
            $this->assertNotEmpty($this->debugEventsResults['onEventStart']);
        } else {
            $expected = [
                'name'   => 'first',
                'args'   => new Args(['donbidon\Core\Event\EventsTest::onA' => TRUE]),
                'source' => 'core:event:debug',
                'level'  => E_NOTICE,
            ];
            $this->assertEquals($expected, $this->debugEventsResults['onEventStart']);
        }
        if (empty($this->debugEventsResults['onHandlerFound'])) {
            $this->assertNotEmpty($this->debugEventsResults['onHandlerFound']);
        } else {
            $expected = [
                'uid'     => $this->uid,
                'name'    => 'first',
                'handler' => [$this, 'onA'],
                'args'    => new Args(['donbidon\Core\Event\EventsTest::onA' => TRUE]),
                'source'  => 'core:event:debug',
                'level'   => E_NOTICE,
            ];
            $this->assertEquals($expected, $this->debugEventsResults['onHandlerFound']);
        }
        if (empty($this->debugEventsResults['onEventEnd'])) {
            $this->assertNotEmpty($this->debugEventsResults['onEventEnd']);
        } else {
            $expected = [
                'uid'     => $this->uid,
                'name'    => 'first',
                'args'    => new Args(['donbidon\Core\Event\EventsTest::onA' => TRUE]),
                'source'  => 'core:event:debug',
                'level'   => E_NOTICE,
            ];
            $this->assertEquals($expected, $this->debugEventsResults['onEventEnd']);
        }

        $args->delete('donbidon\Core\Event\EventsTest::onA');
        $this->evtManager->disableHandler('first');
        $this->evtManager->fire('first', $args);
        if (empty($this->debugEventsResults['onDisableHandler'])) {
            $this->assertNotEmpty($this->debugEventsResults['onDisableHandler']);
        } else {
            $expected = [
                'name'    => 'first',
                'source'  => 'core:event:debug',
                'level'   => E_NOTICE,
            ];
            $this->assertEquals($expected, $this->debugEventsResults['onDisableHandler']);
        }
        $this->evtManager->fire('first', $args);
        $this->assertEquals([], $args->get());

        $this->evtManager->enableHandler('first');
        $this->evtManager->fire('first', $args);
        if (empty($this->debugEventsResults['onEnableHandler'])) {
            $this->assertNotEmpty($this->debugEventsResults['onEnableHandler']);
        } else {
            $expected = [
                'name'    => 'first',
                'source'  => 'core:event:debug',
                'level'   => E_NOTICE,
            ];
            $this->assertEquals($expected, $this->debugEventsResults['onEnableHandler']);
        }
        $this->assertEquals(
            ['donbidon\Core\Event\EventsTest::onA' => TRUE],
            $args->get()
        );

        $this->evtManager->dropHandlers('first');
        $args->delete('donbidon\Core\Event\EventsTest::onA');
        $this->evtManager->fire('first', $args);
        if (empty($this->debugEventsResults['onDropHandlers'])) {
            $this->assertNotEmpty($this->debugEventsResults['onDropHandlers']);
        } else {
            $expected = [
                'name'    => 'first',
                'source'  => 'core:event:debug',
                'level'   => E_NOTICE,
            ];
            $this->assertEquals($expected, $this->debugEventsResults['onEnableHandler']);
        }
        $this->assertEquals([], $args->get());
    }

    /**
     * Hadler for testing event handling.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testExceptionWhenEventFiredAlready()
     */
    public function onA($name, I_Registry $args)
    {
        $args->set(__METHOD__, TRUE);
    }

    /**
     * Hadler for testing event handling.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testExceptionWhenEventFiredAlready()
     */
    public function onB($name, I_Registry $args)
    {
        $args->set(__METHOD__, TRUE);
    }

    /**
     * Hadler for testing event handling.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testExceptionWhenEventFiredAlready()
     */
    public function onLowPriorityA($name, I_Registry $args)
    {
        $args->set(__METHOD__, TRUE);
    }

    /**
     * Hadler for testing event handling.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testExceptionWhenEventFiredAlready()
     */
    public function onLowPriorityB($name, I_Registry $args)
    {
        $args->set(__METHOD__, TRUE);
    }

    /**
     * Hadler for testing event handling.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testExceptionWhenEventFiredAlready()
     */
    public function onHighPriority($name, I_Registry $args)
    {
        $args->set(__METHOD__, TRUE);
    }

    /**
     * Hadler for testing event handling.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testExceptionWhenEventFiredAlready()
     */
    public function onBreakingEvent($name, I_Registry $args)
    {
        $args->set(':break:', TRUE);
    }

    /**
     * Hadler for testing debugging events.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testDebuggingEvents()
     */
    public function onAddHandler($name, I_Registry $args)
    {
        if (!isset($this->debugEventsResults['onAddHandler'])) {
            $this->debugEventsResults['onAddHandler'] = [];
        }
        $this->debugEventsResults['onAddHandler'][] = $args->get();
    }

    /**
     * Hadler for testing debugging events.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testDebuggingEvents()
     */
    public function onEventStart($name, I_Registry $args)
    {
        $this->uid = $args->get('uid');
        $args->delete('uid');
        $this->debugEventsResults['onEventStart'] = $args->get();
    }

    /**
     * Hadler for testing debugging events.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testDebuggingEvents()
     */
    public function onHandlerFound($name, I_Registry $args)
    {
        $this->debugEventsResults['onHandlerFound'] = $args->get();
    }

    /**
     * Hadler for testing debugging events.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testDebuggingEvents()
     */
    public function onEventEnd($name, I_Registry $args)
    {
        $this->debugEventsResults['onEventEnd'] = $args->get();
    }

    /**
     * Hadler for testing debugging events.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testDebuggingEvents()
     */
    public function onDisableHandler($name, I_Registry $args)
    {
        $this->debugEventsResults['onDisableHandler'] = $args->get();
    }

    /**
     * Hadler for testing debugging events.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testDebuggingEvents()
     */
    public function onEnableHandler($name, I_Registry $args)
    {
        $this->debugEventsResults['onEnableHandler'] = $args->get();
    }

    /**
     * Hadler for testing debugging events.
     *
     * @param  sctring    $name
     * @param  I_Registry $args
     * @return void
     * @see    self::testDebuggingEvents()
     */
    public function onDropHandlers($name, I_Registry $args)
    {
        $this->debugEventsResults['onDropHandlers'] = $args->get();
    }
}
