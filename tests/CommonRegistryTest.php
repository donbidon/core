<?php
/**
 * Common registry class unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Registry;

use RuntimeException;

/**
 * Common registry class unit tests.
 */
class CommonRegistryTest extends \donbidon\Lib\PHPUnit\TestCase
{
    /**
     * Initial scope
     *
     * @var array
     */
    protected $initialScope = [
        'key_1'       => "value_1",
        'empty_key_1' => "",
        'empty_key_2' => "0",
        'empty_key_3' => 0,
        'empty_key_4' => NULL,
        'key_2'       => "value_2",
    ];

    /**
     * Registry instance
     *
     * @var Common
     */
    protected $registry;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $this->registry = new Common($this->initialScope);
    }

    /**
     * Tests forbidden create action.
     *
     * @return void
     * @covers donbidon\Core\Common::set
     */
    public function testForbiddenCreate()
    {
        $this->initForbiddenActions();

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("No permissions for ACTION_CREATE");
        $this->expectExceptionCode(Common::EX_CODE_PREFIX | Common::ACTION_CREATE);

        $this->registry->set('new key', "some value");
    }

    /**
     * Tests forbidden modify action.
     *
     * @return void
     * @covers donbidon\Core\Common::set
     */
    public function testForbiddenModify()
    {
        $this->initForbiddenActions();
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("No permissions for ACTION_MODIFY");
        $this->expectExceptionCode(Common::EX_CODE_PREFIX | Common::ACTION_MODIFY);
        $this->registry->set('key', "other value");
    }

    /**
     * Tests forbidden delete action.
     *
     * @return void
     * @covers donbidon\Core\Common::delete
     */
    public function testForbiddenDelete()
    {
        $this->initForbiddenActions();
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("No permissions for ACTION_DELETE");
        $this->expectExceptionCode(Common::EX_CODE_PREFIX | Common::ACTION_DELETE);
        $this->registry->delete('key');
    }

    /**
     * Tests forbidden override action.
     *
     * @return void
     * @covers donbidon\Core\Common::override
     */
    public function testForbiddenOverride()
    {
        $this->initForbiddenActions();
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("No permissions for ACTION_OVERRIDE");
        $this->expectExceptionCode(Common::EX_CODE_PREFIX | Common::ACTION_OVERRIDE);
        $this->registry->override([]);
    }

    /**
     * Tests exception when missing key and no default value passed.
     *
     * @return void
     * @covers donbidon\Core\Common::get
     */
    public function testUnexistentKey()
    {
        $this->initForbiddenActions();
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("Missing key 'unexistent_key'");
        $this->registry->get('unexistent_key');
    }

    /**
     * Tests common functionality.
     *
     * @return void
     * @covers donbidon\Core\Common::get
     * @covers donbidon\Core\Common::set
     * @covers donbidon\Core\Common::delete
     * @covers donbidon\Core\Common::exists
     * @covers donbidon\Core\Common::isEmpty
     */
    public function testCommonFunctionality()
    {
        $this->assertEquals("value_1",           $this->registry->get('key_1'));
        $this->assertEquals(100500,              $this->registry->get('key_3', 100500));
        $this->assertEquals("",                  $this->registry->get('empty_key_1'));
        $this->assertEquals("0",                 $this->registry->get('empty_key_2'));
        $this->assertEquals(0,                   $this->registry->get('empty_key_3'));
        $this->assertEquals(NULL,                $this->registry->get('empty_key_4'));
        $this->assertEquals($this->initialScope, $this->registry->get());

        $this->registry->set('key_1', "value_1_1");
        $this->assertEquals("value_1_1", $this->registry->get('key_1'));

        $this->registry->delete('key_1');
        $this->assertFalse($this->registry->exists('key_1'));
        $this->assertTrue($this->registry->isEmpty('key_1'));

        $this->assertTrue($this->registry->isEmpty('key_3'));
        $this->assertTrue($this->registry->isEmpty('empty_key_1'));
        $this->assertTrue($this->registry->isEmpty('empty_key_2'));
        $this->assertTrue($this->registry->isEmpty('empty_key_3'));
        $this->assertTrue($this->registry->isEmpty('empty_key_4'));
    }

    /**
     * Tests override.
     *
     * @return void
     * @covers donbidon\Core\Common::override
     */
    public function testOverride()
    {
        $this->registry->override(['key_1' => "value_1*"]);
        $this->assertEquals("value_1*", $this->registry->get('key_1'));
    }

    /**
     * Tests static instance.
     *
     * @return void
     * @covers donbidon\Core\Common::override
     */
    public function testStaticInstance()
    {
        UTCommonRegistry::resetInstance();

        $registry = UTCommonRegistry::getInstance(['key' => "static instance"]);

        $this->assertTrue(
            $registry instanceof Common,
            sprintf("Class %s isn't instance of Registry", get_class($registry))
        );
        $this->assertEquals(
            "static instance",
            $registry->get('key'),
            "Static instance contains wrong scope"
        );

        UTCommonRegistry::resetInstance();
    }

    /**
     * Initializes registry instance having no actions allowed.
     *
     * @return   void
     * @internal
     */
    protected function initForbiddenActions()
    {
        $this->checkGroupIfSkipped('all');
        $this->registry = new Common(
            [
                'key' => "value",
            ],
            Common::ACTION_NONE
        );
    }
}
