<?php
/**
 * Bootstrap class unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core;

use PHPUnit\Framework\Error\Notice;
use PHPUnit\Framework\Error\Warning;
use donbidon\Core\Registry\UTRecursiveRegistry;

/**
 * Bootstrap class unit tests.
 */
class BootstrapTest extends \donbidon\Lib\PHPUnit\TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        UTRecursiveRegistry::resetInstance();
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();

        UTRecursiveRegistry::resetInstance();
    }

    /**
     * Tests exception when passed wrong path.
     *
     * @return void
     * @covers donbidon\Core\Bootstrap::initByPath
     */
    public function testInitByWrongPath()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(
            Bootstrap::EX_CODE_PREFIX | Bootstrap::EX_CANNOT_OPEN_CONFIG
        );
        $this->expectExceptionMessage(
            "Cannot open config file \"/unexistent/path\""
        );
        UTBootstrap::initByPath("/unexistent/path");
    }

    /**
     * Tests exception when passed wrong path.
     *
     * @return void
     * @covers donbidon\Core\Bootstrap::initByPath
     */
    public function testInitByPathContainigInvalidConfig()
    {
        $this->expectException(Warning::class);
        $this->expectExceptionMessage(
            "syntax error, unexpected '^' in Unknown on line 2"
        );
        UTBootstrap::initByPath("../tests.data/BootstrapTest.invalid-config.php");
    }

    /**
     * Tests functionality.
     *
     * @return void
     * @covers donbidon\Core\Bootstrap::initByPath
     * @covers donbidon\Core\Bootstrap::initByArray
     */
    public function testFunctionality()
    {
        UTBootstrap::initByPath("../tests.data/BootstrapTest.config.php");
        $this->assertTrue(
            UTRecursiveRegistry::_get('core/event/manager') instanceof
            \donbidon\Core\Event\Manager
        );
        UTRecursiveRegistry::_delete('core/event/manager');

        $expected = [
            'core' => [
                'event' => [
                    'debug' => "1",
                ],
                'log' => [
                    'method' => "UTLogMethod",
                    'level'  => (string)(E_ERROR | E_WARNING | E_NOTICE),
                    'source' => [
                        "LogTest::someMethod()",
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, UTRecursiveRegistry::_get());
    }
}
