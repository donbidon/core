<?php
/**
 * Recursive registry class unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Registry;

use RuntimeException;

/**
 * Recursive registry class unit tests.
 */
class RecursiveRegistryTest extends \donbidon\Lib\PHPUnit\TestCase
{
    /**
     * Initial scope
     *
     * @var array
     */
    protected $initialScope = [
        'key_1'       => "value_1",
        'key_2'       => [
            'key_2_1'       => "value_2_1",
            'key_2_2'       => "value_2_2",
            'empty_key_2_1' => null,
        ],
        'empty_key_3' => "",
    ];

    /**
     * Registry instance
     *
     * @var Recursive
     */
    protected $registry;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $this->registry = new Recursive($this->initialScope);
    }

    /**
     * Tests common functionality.
     *
     * @return void
     * @covers donbidon\Core\Registry::get
     * @covers donbidon\Core\Registry::set
     * @covers donbidon\Core\Registry::delete
     * @covers donbidon\Core\Registry::exists
     * @covers donbidon\Core\Registry::isEmpty
     */
    public function testCommonFunctionality()
    {
        $this->registry->set('key_1/key_1_1', "value_1_1");
        $this->registry->set('key_2/key_2_3', "value_2_3");

        $this->assertEquals(
            ['key_1_1' => "value_1_1", ],
            $this->registry->get('key_1')
        );
        $this->assertEquals(
            [
                'key_2_1'       => "value_2_1",
                'key_2_2'       => "value_2_2",
                'key_2_3'       => "value_2_3",
                'empty_key_2_1' => null,
            ],
            $this->registry->get('key_2')
        );
        $this->assertEquals(
            100500,
            $this->registry->get('key_3', 100500)
        );

        $this->assertTrue($this->registry->exists('key_1'));
        $this->assertTrue($this->registry->exists('key_1/key_1_1'));
        $this->assertTrue($this->registry->exists('key_2/empty_key_2_1'));
        $this->assertFalse($this->registry->exists('key_1/key_1_2'));
        $this->assertFalse($this->registry->exists('key_2/key_2_4'));
        $this->assertFalse($this->registry->exists('key_3'));
        $this->assertFalse($this->registry->exists('key_4/key_4_1'));
        $this->assertFalse($this->registry->exists('key_5/key_5_1/key_5_1_1'));

        $this->assertFalse($this->registry->isEmpty('key_1'));
        $this->assertFalse($this->registry->isEmpty('key_1/key_1_1'));
        $this->assertTrue($this->registry->isEmpty('key_1/key_1_2'));
        $this->assertTrue($this->registry->isEmpty('key_2/empty_key_2_1'));
        $this->assertTrue($this->registry->isEmpty('key_2/key_2_4'));
        $this->assertTrue($this->registry->isEmpty('key_3'));
        $this->assertTrue($this->registry->isEmpty('key_4/key_4_1'));
        $this->assertTrue($this->registry->isEmpty('key_5/key_5_1/key_5_1_1'));
    }

    /**
     * Tests static instance.
     *
     * @return void
     * @covers donbidon\Core\Registry::override
     */
    public function testStaticInstance()
    {
        UTRecursiveRegistry::resetInstance();

        $registry = UTRecursiveRegistry::getInstance(['key' => "static instance"]);

        $this->assertTrue(
            $registry instanceof Recursive,
            sprintf("Class %s isn't instance of Registry", get_class($registry))
        );
        $this->assertEquals(
            "static instance",
            $registry->get('key'),
            "Static instance contains wrong scope"
        );

        UTRecursiveRegistry::resetInstance();
    }
}
