<?php
/**
 * Logger method class extension for unit testing.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Log\Method;

use donbidon\Core\Registry\UTRecursiveRegistry;
use donbidon\Core\Log\A_Logger;
use donbidon\Core\Event\Args;

/**
 * Logger method class extension for unit testing.
 */
class UTLogMethod extends A_Logger
{
    /**
     * {@inheritdoc}
     *
     * @param Args $args
     */
    protected function log(Args $args)
    {
        $log = UTRecursiveRegistry::_get('log/unittest', []);
        $log[] = $args->get();
        UTRecursiveRegistry::_set('log/unittest', $log);
    }
}
