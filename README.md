# Core

## Installing
Run ` composer --dev require donbidon/core dev-master ` or add following code to your "composer.json" file:
```json
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/donbidon/core"
        }
    ],
    "require": {
        "donbidon/core": "dev-master"
    }
```
and run `composer update`.

## Usage
### Starting up full core environment
"config.php" file:
```
;<?php die; __halt_compiler();
[core]

; By default: Off
event[debug] = On

; By default: "StdOut"
;log[method] = "File"

; By default: E_ERROR | E_WARNING
log[level] = E_ERROR | E_WARNING | E_NOTICE

log.source[] = "*" ; means all log sources
;log.source[] = "Foo::someMethod()"

; log[path]     = "..." ; For file logger, see donbidon\Lib\FileSystem\Logger
; log[maxSize]  = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
; log[rotation] = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
; log[righs]    = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
```
PHP-executable:
```php
<?php

require_once "/path/to/vendor/autoload.php";

\donbidon\Core\Bootstrap::initByPath("/path/to/config");

// Your code...
```
