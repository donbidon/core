<?php
/**
 * Event arguments.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Core\Event;

/**
 * Event arguments.
 *
 * @see Manager
 */
class Args implements \donbidon\Core\Registry\I_Registry
{
    /**
     * Scope of arguments
     *
     * @var array
     */
    protected $args;

    /**
     * Constructor.
     *
     * @param array $args  Initial arguments
     */
    public function __construct(array $args = [])
    {
        $this->args = $args;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @param  mixed  $value
     * @return void
     */
    public function set($key, $value)
    {
        $this->args[$key] = $value;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @return bool
     */
    public function exists($key)
    {
        $result = array_key_exists($key, $this->args);

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @return bool
     * @link   http://php.net/manual/en/function.empty.php
     */
    public function isEmpty($key)
    {
        $result = empty($this->args[$key]);

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key     If not passed, all arguments will be returned
     * @param  mixed  $default
     * @return mixed
     */
    public function get($key = NULL, $default = NULL)
    {
        if (is_null($key)) {
            $result = $this->args;
        } else {
            $result = $this->exists($key) ? $this->args[$key] : $default;
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @return void
     */
    public function delete($key)
    {
        unset($this->args[$key]);
    }
}
