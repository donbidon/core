<?php
/**
 * Recursive registry functionality.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Registry;

use RuntimeException;

/**
 * Recursive registry functionality.
 *
 * PHP-executable:
 * ```php
 * $registry = new \donbidon\Core\Registry\Recursive([
 *     'key_1' => "value_1",
 *     'key_2' => [
 *         'key_2_1' => "value_2_1",
 *         'key_2_2' => "value_2_2",
 *     ],
 * ]);
 * var_dump($registry->exists('key_1'));
 * var_dump($registry->exists('key_2/key_2_3'));
 * $registry->set('key_2/key_2_2/key_2_2_1', "value_2_2_1");
 * var_dump($registry->get());
 * ```
 * Output:
 * ```
 * bool(true)
 * bool(false)
 * array(2) {
 *   ["key_1"]=>
 *   string(7) "value_1"
 *   ["key_2"]=>
 *   array(2) {
 *     ["key_2_1"]=>
 *     string(9) "value_2_1"
 *     ["key_2_2"]=>
 *     array(1) {
 *       ["key_2_2_1"]=>
 *       string(11) "value_2_2_1"
 *     }
 *   }
 * }
 * ```
 */
class Recursive extends Common
{
    /**
     * Full scope, temporary scope according to complex key
     * will be stored in self::$scope
     *
     * @var array
     */
    protected $fullScope;

    /**
     * Key delimiter
     *
     * @var string
     */
    protected $delimiter;

    /**
     * Constructor.
     *
     * @param array  $scope
     * @param int    $options
     * @param string $delimiter  Key delimiter
     */
    public function __construct(
        array $scope = [],
        $options = self::ACTION_ALL,
        $delimiter = '/'
    )
    {
        $this->fullScope = $scope;
        $this->delimiter = $delimiter;

        parent::__construct($scope, $options);
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @param  mixed  $value
     */
    public function set($key, $value)
    {
        $this->setScope($key, TRUE);

        parent::set($key, $value);
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     */
    public function exists($key)
    {
        $this->setScope($key);
        $result = is_array($this->scope) ? parent::exists($key) : FALSE;

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     */
    public function isEmpty($key)
    {
        $this->setScope($key);
        $result = parent::isEmpty($key);

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @param  mixed  $default
     * @param  bool   $throw   Throow exception if no default value passed and
     *                         key doesn't exist
     */
    public function get($key = NULL, $default = NULL, $throw = TRUE)
    {
        $this->setScope($key);
        $result = parent::get($key, $default, $throw);

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     */
    public function delete($key)
    {
        $this->setScope($key);
        parent::delete($key);
    }

    /**
     * Shifts scope according to complex key.
     *
     * @param    string $key    <b>[by ref]</b>
     * @param    bool   $create
     * @return   void
     * @internal
     */
    protected function setScope(&$key, $create = FALSE)
    {
        $this->scope = &$this->fullScope;
        if (FALSE === strpos($key, $this->delimiter)) {
            return;
        }
        $this->skipSettingScope = TRUE;
        $keys = explode($this->delimiter, $key);
        $lastName = array_pop($keys);
        $lastIndex = sizeof($keys) - 1;
        foreach ($keys as $index => $key) {
            if (!isset($this->scope[$key]) || !is_array($this->scope[$key])) {
                if ($create) {
                    $this->scope[$key] = [];
                } else if (!isset($this->scope[$key]) && $index == $lastIndex) {
                    return;
                }
            }
            $this->scope = &$this->scope[$key];
        }
        $key = $lastName;
    }
}
