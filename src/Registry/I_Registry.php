<?php
/**
 * Registry interface.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Registry;

/**
 * Registry interface.
 */
interface I_Registry
{
    /**
     * Sets scope value.
     *
     * @param  string $key
     * @param  mixed  $value
     * @return void
     */
    public function set($key, $value);

    /**
     * Returns TRUE if scope exists, FALSE otherwise.
     *
     * @param  string $key
     * @return bool
     */
    public function exists($key);

    /**
     * Returns TRUE if scope value is empty, FALSE otherwise.
     *
     * @param  string $key
     * @return bool
     * @link   http://php.net/manual/en/function.empty.php
     */
    public function isEmpty($key);
    /**
     * Returns scope value.
     *
     * @param  string $key     If not passed, whole scope will be returned
     * @param  mixed  $default
     * @return mixed
     */
    public function get($key = NULL, $default = NULL);

    /**
     * Deletes scope key.
     *
     * @param  string $key
     * @return void
     */
    public function delete($key);
}
