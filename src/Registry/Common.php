<?php
/**
 * Static and non-static simple non-recursive registry functionality.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Registry;

use RuntimeException;
use donbidon\Core\Friendlification;

/**
 * Static and non-static simple non-recursive registry functionality.
 *
 * PHP-executable:
 * ```php
 * $registry = new \donbidon\Core\Registry\Common[
 *     'key_1' => "value_1",
 *     'key_2' => "value_2",
 * ]);
 * var_dump($registry->exists('key_1'));
 * var_dump($registry->exists('key_3'));
 * $registry->set('key_3', "value_3");
 * var_dump($registry->get());
 * ```
 *
 * Output:
 * ```
 * bool(true)
 * bool(false)
 * array(3) {
 *   ["key_1"]=>
 *   string(7) "value_1"
 *   ["key_2"]=>
 *   string(7) "value_2"
 *   ["key_3"]=>
 *   string(7) "value_2"
 * }
 * ```
 */
class Common implements I_Registry
{
    /**
     * Disallow all actions
     *
     * @see self::__construct()
     */
    const ACTION_NONE = 0;

    /**
     * Allow to create new keys
     *
     * @see self::__construct()
     */
    const ACTION_CREATE   = 0x0001;

    /**
     * Allow to modify existing keys
     *
     * @see self::__construct()
     */
    const ACTION_MODIFY   = 0x0002;

    /**
     * Allow to delete keys
     *
     * @see self::__construct()
     */
    const ACTION_DELETE   = 0x0004;

    /**
     * Allow to override scope
     *
     * @see self::__construct()
     */
    const ACTION_OVERRIDE = 0x0008;

    /**
     * Allow all actions
     *
     * @see self::__construct()
     */
    const ACTION_ALL      = 0x000F;

    /**
     * Exception code prefix
     *
     * @see self::checkPermissions()
     */
    const EX_CODE_PREFIX  = 0x1000;

    /**
     * Static registry instance
     *
     * @var self
     */
    protected static $instance;

    /**
     * Scope
     *
     * @var array
     */
    protected $scope;

    /**
     * Initial options
     *
     * @var int
     */
    protected $options;

    /**
     * Returns static registry instance.
     *
     * @param  array $scope
     * @param  int   $options
     * @return static
     */
    public static function getInstance(array $scope = [], $options = self::ACTION_ALL)
    {
        if (!is_object(self::$instance)) {
            self::$instance = new static($scope, $options);
        }

        return self::$instance;
    }

    /**
     * Short alias for self::getInstance()->set().
     *
     * @param  string $key
     * @param  mixed  $value
     * @return void
     * @see    self::set()
     */
    public static function _set($key, $value)
    {
        self::getInstance()->set($key, $value);
    }

    /**
     * Short alias for self::getInstance()->exists().
     *
     * @param  string $key
     * @return bool
     * @see    self::exists()
     */
    public static function _exists($key)
    {
        return self::getInstance()->exists($key);
    }

    /**
     * Short alias for self::getInstance()->isEmpty().
     *
     * @param  string $key
     * @return bool
     * @see    self::exists()
     */
    public static function _isEmpty($key)
    {
        return self::getInstance()->isEmpty($key);
    }

    /**
     * Short alias for self::getInstance()->get().
     *
     * @param  string $key
     * @param  mixed  $default
     * @return mixed
     * @see    self::get()
     */
    public static function _get($key = NULL, $default = NULL)
    {
        return self::getInstance()->get($key, $default);
    }

    /**
     * Short alias for self::getInstance()->delete().
     *
     * @param  string $key
     * @return void
     * @see    self::delete()
     */
    public static function _delete($key)
    {
        self::getInstance()->delete($key);
    }

    /**
     * Short alias for self::getInstance()->override().
     *
     * @param  array $scope
     * @return void
     * @see    self::override()
     */
    public static function _override(array $scope)
    {
        self::getInstance()->override($scope);
    }

    /**
     * Constructor.
     *
     * Example:
     * ```php
     * use donbidon\Core\Registry\Common;
     *
     * // Create registry allowing to add new keys only
     * $registry = new Common(
     *     [
     *         'key_1' => "value_1",
     *     ],
     *     Common::ACTION_CREATE
     * );
     *
     * $registry->set('key_2', "value_2"); // Ok
     *
     * // RuntimeException having Registry::EX_CODE_PREFIX | Registry::ACTION_CREATE
     * // code will be thrown.
     * $registry->set('key_2', "value_2*");
     * ```
     *
     * @param  array $scope
     * @param  int   $options  Combination of the following flags:
     *                         - self::ACTION_CREATE,
     *                         - self::ACTION_DELETE,
     *                         - self::ACTION_MODIFY,
     *                         - self::ACTION_OVERRIDE
     */
    public function __construct(
        array $scope = [],
        $options = self::ACTION_ALL
    )
    {
        $this->scope   = (array)$scope;
        $this->options = (int)$options;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @param  mixed  $value
     * @return void
     */
    public function set($key, $value)
    {
        $this->checkPermissions(
            array_key_exists($key, $this->scope)
                ? self::ACTION_MODIFY
                : self::ACTION_CREATE
        );
        $this->scope[$key] = $value;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @return bool
     */
    public function exists($key)
    {
        $result = array_key_exists($key, $this->scope);

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @return bool
     * @link   http://php.net/manual/en/function.empty.php
     */
    public function isEmpty($key)
    {
        $result = empty($this->scope[$key]);

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key     If not passed, whole scope will be returned
     * @param  mixed  $default
     * @param  bool   $throw   Throow exception if no default value passed and
     *                         key doesn't exist
     * @return mixed
     * @throws RuntimeException  If key doesn't exist.
     */
    public function get($key = NULL, $default = NULL, $throw = TRUE)
    {
        $result = $default;

        if (is_null($key)) {
            $result = $this->scope;
        } else if (is_array($this->scope) && array_key_exists($key, $this->scope)) {
            $result = $this->scope[$key];
        } else if (is_null($default) && $throw) {
            throw new RuntimeException(sprintf(
                "Missing key '%s'", $key
            ));
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $key
     * @return void
     */
    public function delete($key)
    {
        $this->checkPermissions(self::ACTION_DELETE);
        unset($this->scope[$key]);
    }

    /**
     * Overrides scope.
     *
     * @param  array $scope
     * @return void
     * @throws
     */
    public function override(array $scope)
    {
        $this->checkPermissions(self::ACTION_OVERRIDE);
        $this->scope = $scope;
    }

    /**
     * Chceck action permissions.
     *
     * @param  int $action
     * @return void
     * @throws RuntimeException  If doesn't have permissions for passed action
     */
    protected function checkPermissions($action)
    {
        if (!($action & $this->options)) {
            throw new RuntimeException(
                sprintf(
                    "No permissions for %s",
                    Friendlification::getConstNameByValue(__CLASS__, $action)
                ),
                self::EX_CODE_PREFIX | $action
            );
        }
    }
}
