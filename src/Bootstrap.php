<?php
/**
 * Core initialization.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core;

use InvalidArgumentException;
use RuntimeException;
use donbidon\Core\Registry\I_Registry;
use donbidon\Core\Registry\Common;


/**
 * Core environment initialization.
 *
 * "config.php" file:
 * ```
 * ;<?php die; __halt_compiler();
 * [core]
 *
 * ; By default: Off
 * event[debug] = On
 *
 * ; By default: "StdOut"
 * ;log[method] = "StdOut"
 *
 * ; By default: E_ERROR | E_WARNING
 * log[level] = E_ERROR | E_WARNING | E_NOTICE
 *
 * log.source[] = "*" ; means all log sources
 * ;log.source[] = "Foo::someMethod()"
 * ```
 * PHP-executable:
 * ```php
 * \donbidon\Core\Bootstrap::initByPath("/path/to/config");
 * ```
 */
class Bootstrap
{
    /**
     * Exception code
     *
     * @see self::initByPath()
     */
    const EX_CANNOT_OPEN_CONFIG  = 0x01;

    /**
     * Exception code
     *
     * @see self::initByPath()
     */
    const EX_CANNOT_PARSE_CONFIG = 0x02;

    /**
     * Exception code
     *
     * @see self::initByArray()
     */
    const EX_INVALID_ARG         = 0x04;

    /**
     * Exception code prefix
     */
    const EX_CODE_PREFIX = 0x1000;

    /**
     * Initializes environment by config file path.
     *
     * @param  string $path
     * @param  int    $options
     * @return Common
     * @throws RuntimeException If passed file doesn't exist or cannot be read.
     * @throws RuntimeException In case of impossibility of parsings config file.
     */
    public static function initByPath($path, $options = Common::ACTION_ALL)
    {
        if (!file_exists($path) || !is_readable($path)) {
            throw new RuntimeException(
                sprintf("Cannot open config file \"%s\"", $path),
                self::EX_CODE_PREFIX | self::EX_CANNOT_OPEN_CONFIG
            );
        }
        $config = \donbidon\Lib\Config\Ini::parse(file_get_contents($path), TRUE);
        if (!is_array($config)) {
            throw new RuntimeException(
                sprintf("Cannot parse config file \"%s\"", $path),
                self::EX_CODE_PREFIX | self::EX_CANNOT_PARSE_CONFIG
            );
        }
        $registry = static::initByArray($config, $options);

        return $registry;
    }

    /**
     * Initializes environment by config array.
     *
     * @param  array $config
     * @return Common
     * @param  int    $options  {@see Recursive::__construct()}
     * @throws InvalidArgumentException
     */
    public static function initByArray(array $config, $options = Common::ACTION_ALL)
    {
        if (!is_array($config)) {
            throw new InvalidArgumentException(
                sprintf("Passed argement isn't array (%s)", gettype($config)),
                self::EX_CODE_PREFIX | self::EX_INVALID_ARG
            );
        }
        $registry = static::getRegistry($config, $options);
        static::modyfyRegistryOnStart($registry);
        $evtManager = new Event\Manager;
        $evtManager->setDebug($registry->get('core/event/debug', FALSE));
        Log\Factory::run($registry, $evtManager);
        $registry->set('core/event/manager', $evtManager);

        return $registry;
    }

    /**
     * Returns registry instance.
     *
     * Called from {@see static::initByArray() here}.
     *
     * @param  array $config
     * @param  int   $options
     * @return I_Registry
     * @see    self::initByArray()
     */
    protected static function getRegistry(array $config, $options)
    {
        $result = \donbidon\Core\Registry\Recursive::getInstance(
            $config,
            $options
        );

        return $result;
    }

    /**
     * Stub method called after getting registry.
     *
     * @param  I_Registry $registry
     * @return void
     * @see    self::initByArray()
     */
    protected static function modyfyRegistryOnStart(I_Registry $registry)
    {
    }
}
