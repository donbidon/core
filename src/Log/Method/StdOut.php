<?php
/**
 * StdOut logger.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Core\Log\Method;

use donbidon\Core\Log\A_Logger;
use donbidon\Core\Event\Args;

/**
 * StdOut logger.
 *
 * See  {@see \donbidon\Core\Log\T_Logger} for usage description.
 *
 * @todo Cover by unit tests.
 */
class StdOut extends A_Logger
{
    /**
     * {@inheritdoc}
     *
     * @param Args $args
     */
    protected function log(Args $args)
    {
        $message = sprintf("%s%s", $this->render($args), PHP_EOL);

        echo $message;
    }
}
