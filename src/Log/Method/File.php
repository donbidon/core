<?php
/**
 * File logger.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Core\Log\Method;

use donbidon\Core\Registry\I_Registry;
use donbidon\Core\Log\A_Logger;
use donbidon\Core\Event\Args;
use donbidon\Core\Event\Manager;
use donbidon\Lib\FileSystem\Logger;

/**
 * File logger.
 *
 * See  {@see \donbidon\Core\Log\T_Logger} for usage description.
 */
class File extends A_Logger
{
    /**
     * Logger instance
     *
     * @var Logger
     */
    protected $logger;

    /**
     * {@inheritdoc}
     *
     * @param I_Registry $registry
     * @param Manager    $evtManager
     */
    public function __construct(I_Registry $registry, Manager $evtManager)
    {
        $this->logger = new Logger($registry->get('core/log'));

        parent::__construct($registry, $evtManager);
    }

    /**
     * {@inheritdoc}
     *
     * @param  Args   $args
     */
    protected function log(Args $args)
    {
        $message = sprintf("%s%s", $this->render($args), PHP_EOL);
        $this->logger->log($message);
    }
}
