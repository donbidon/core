<?php
/**
 * Logger factory.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Log;

use donbidon\Core\Registry\I_Registry;
use donbidon\Core\Event\Manager;

/**
 * Logger factory.
 *
 * See {@see \donbidon\Core\Log\T_Logger} for usage description.
 */
class Factory
{
    /**
     * Default logger
     *
     * @var string
     */
    protected static $default = 'StdOut';

    /**
     * Factory.
     *
     * Creates and returns logger instance.
     *
     * @param  I_Registry $registry
     * @param  Manager    $evtManager
     * @return A_Logger
     */
    public static function run(I_Registry $registry, Manager $evtManager)
    {
        $class = sprintf(
            "\\donbidon\\Core\\Log\\Method\\%s",
            $registry->get('core/log/method', self::$default)
        );
        $instance = new $class($registry, $evtManager);

        return $instance;
    }
}
