<?php
/**
 * Logger trait.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Log;

use donbidon\Core\ExceptionExtended;
use donbidon\Core\Event\Args;
use donbidon\Core\Event\Manager;

/**
 * Logger trait.
 *
 * "config.php" file:
 * ```
 * ;<?php die; __halt_compiler();
 * [core]
 *
 * ; By default: Off
 * event[debug] = On
 *
 * ; By default: "StdOut"
 * ;log[method] = "StdOut"
 *
 * ; By default: E_ERROR | E_WARNING
 * log[level] = E_ERROR | E_WARNING | E_NOTICE
 *
 * ;log.source[] = "*" ; means all log sources
 * log.source[] = "Foo::someMethod()"
 * ```
 * PHP-executable:
 * ```php
 * use donbidon\Core\Bootstrap;
 * use donbidon\Core\Registry\Recursive;
 * use donbidon\Core\Log\T_Logger;
 *
 * Bootstrap::initByPath("/path/to/config");
 *
 * class Foo
 * {
 *     use T_Logger;
 *
 *     public function __construct()
 *     {
 *         $this->evtManager = Recursive::_get('core/event/manager');
 *     }
 *
 *     public function someMethod()
 *     {
 *         $this->log("notice",  'Foo::someMethod()', E_NOTICE);
 *         $this->log("warning", 'Foo::someMethod()', E_WARNING);
 *         $this->log("error",   'Foo::someMethod()', E_ERROR);
 *     }
 *
 *     public function otherMethod()
 *     {
 *         $this->log("warinig from other method",  'Foo::otherMethod()', E_WARNING);
 *     }
 * }
 *
 * $registry = Recursive::getInstance();
 * $foo = new Foo;
 * $foo->someMethod();
 * $foo->otherMethod();
 * $registry->delete('core/log/level');
 * echo PHP_EOL;
 * $foo->someMethod();
 * $source = $registry->get('core/log/source');
 * $source[] = "*";
 * $registry->set('core/log/source', $source);
 * echo PHP_EOL;
 * $foo->otherMethod();
 * ```
 * Output:
 * ```
 * [ YYYY-MM-DD **:**:** ][ note ][ Foo::someMethod() ] ~ notice
 * [ YYYY-MM-DD **:**:** ][ WARN ][ Foo::someMethod() ] ~ warning
 * [ YYYY-MM-DD **:**:** ][ ERR  ][ Foo::someMethod() ] ~ error
 *
 * [ YYYY-MM-DD **:**:** ][ WARN ][ Foo::someMethod() ] ~ warning
 * [ YYYY-MM-DD **:**:** ][ ERR  ][ Foo::someMethod() ] ~ error
 *
 * [ YYYY-MM-DD **:**:** ][ WARN ][ Foo::otherMethod() ] ~ warning from other method
 * ```
 */
trait T_Logger
{
    /**
     * Event manager instance.
     *
     * @var Manager
     */
    protected $evtManager;

    /**
     * Fires event to log message.
     *
     * @param  string $message
     * @param  string $source
     * @param  int    $level
     * @param  mixed  $data     Exception data
     * @return void
     * @throws ExceptionExtended  If no event manager present.
     */
    protected function log($message, $source = NULL, $level = E_NOTICE, $data = NULL)
    {
        if (!($this->evtManager instanceof Manager)) {
            $e = new ExceptionExtended(
                "T_Logger::\$evtManager isn't instance of \\donbidon\\Core\\Event\\Manager"
            );
            $e->setData($data);
            throw $e;
        }
        if (is_null($source)) {
            $source = ":missed:";
        }
        $args = new Args([
            'message' => $message,
            'source'  => $source,
            'level'   => $level,
        ]);
        $this->evtManager->fire(':log:', $args);
    }
}
