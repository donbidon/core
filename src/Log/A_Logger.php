<?php
/**
 * Logger abstract class.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core\Log;

use donbidon\Core\Registry\I_Registry;
use donbidon\Core\Event\Manager;
use donbidon\Core\Event\Args;

/**
 * Logger abstract class.
 *
 * See {@see \donbidon\Core\Log\T_Logger} for usage description.
 */
abstract class A_Logger
{
    /**
     * Level to string conversation.
     *
     * @var array
     */
    protected $levelToString = [
        E_NOTICE  => 'note',
        E_WARNING => 'WARN',
        E_ERROR   => 'ERR ',
    ];

    /**
     * Registry instance
     *
     * @var I_Registry
     */
    protected $registry;

    /**
     * Event manager instance.
     *
     * @var Manager
     */
    protected $evtManager;

    /**
     * Initilizes logger accordimg to environment.
     *
     * @param I_Registry $registry
     * @param Manager    $evtManager
     */
    public function __construct(I_Registry $registry, Manager $evtManager)
    {
        $this->registry = $registry;
        $this->init($evtManager);
    }

    /**
     * Logging handler.
     *
     * @param  string $name
     * @param  Args   $args
     * @return void
     */
    public function handler($name, Args $args)
    {
        if (
            !$this->checkLevel($args->get('level')) ||
            !$this->checkSource($args->get('source'))
        ) {
            return;
        }

        switch ($name) {
            /*
            case ':log:':
                $this->log($args);
                break; // case 'log'

            */
            case ':onAddHandler:':
                $args->set('message', sprintf(
                    "Adding handler %s, %s",
                    $this->handlerToString($args->get('handler'), $args->get('name')),
                    $args->get('added', FALSE) ? 'successfully' : 'failed'
                ));
                break; // case ':onAddHandler:'

            case ':onEventStart:':
                $args->set('message', sprintf(
                    "{ '%s' (%s), args [%s]",
                    $args->get('name'),
                    $args->get('uid'),
                    implode(', ', array_keys($args->get()))
                ));
                break; // case ':onEventStart:'

            case ':onHandlerFound:':
                $args->set('message', sprintf(
                    "Handler %s found",
                    $this->handlerToString($args->get('handler'))
                ));
                break; // case ':onAddHandler:'

            case ':onEventEnd:':
                $args->set('message', sprintf(
                    "} '%s' (%s), args [%s]",
                    $args->get('name'),
                    $args->get('uid'),
                    implode(', ', array_keys($args->get()))
                ));
                break; // case ':onEventEnd:'

            case ':onDropHandlers:':
                $args->set('message', sprintf(
                    "Dropping handler %s",
                    $this->handlerToString($args->get('handler'), $args->get('name'))
                ));
                break; // case ':onDropHandlers:'

            case ':onDisableHandler:':
                $args->set('message', sprintf(
                    "Disabling handler %s",
                    $this->handlerToString($args->get('handler'), $args->get('name'))
                ));
                break; // case ':onDropHandlers:'

            case ':onEnableHandler:':
                $args->set('message', sprintf(
                    "Enabling handler %s",
                    $this->handlerToString($args->get('handler'), $args->get('name'))
                ));
                break; // case ':onDropHandlers:'
        }
        $this->log($args);
    }

    /**
     * Logger.
     *
     * @param  Args   $args
     * @return void
     */
    protected abstract function log(Args $args);

    /**
     * Inializes event handler to log messages.
     *
     * @param  Manager $evtManager
     * @return void
     */
    protected function init(Manager $evtManager)
    {
        $this->evtManager = $evtManager;
        $events = $this->evtManager->getDebugEvents();
        foreach ($events as $event) {
            $this->evtManager->addHandler($event, [$this, 'handler']);
        }
    }

    /**
     * Returns TRUE if passed source published in config file.
     *
     * @param  string $source
     * @return bool
     */
    protected function checkSource($source)
    {
        $sources = $this->registry->get('core/log/source', []);
        $result  = in_array($source, $sources) || in_array('*', $sources);

        return $result;
    }

    /**
     * Returns TRUE if passed level published in config file.
     *
     * @param  int $level
     * @return bool
     */
    protected function checkLevel($level)
    {
        $default = E_ERROR | E_WARNING;
        $cfgLevel = $this->registry->get('core/log/level', $default);
        if (is_numeric($cfgLevel)) {
            $cfgLevel = (int)$cfgLevel;
        } else if (is_string($cfgLevel) && preg_match('/^[A-Z_&|^ ]+$/', $cfgLevel)) {
            $cfgLevel = eval("return {$cfgLevel};");
        } else {
            $cfgLevel = $default;
        }
        $result = (bool)($cfgLevel & $level);

        return $result;
    }

    /**
    * Returns stringified event handler.
    *
    * @param  callable $handler
    * @param  string   $name
    * @return string
    */
    protected function handlerToString($handler, $name = '')
    {
        $result = '';
        if (is_array($handler)) {
            if (is_object($handler[0])) {
                $class = get_class($handler[0]);
                $call = '->';

            } else {
                $class = $handler[0];
                $call = '::';
            }
            $result = "{$class}{$call}{$handler[1]}";
        } else {
            $result = 'function' . ('' != $handler ? " {$handler}" : '');
        }
        $result .= "({$name})";

        return $result;
    }

    /**
     * Renders message according ro format.
     *
     * @param  Args $args
     * @return string
     * @todo   Support log environment (configurable).
     */
    protected function render(Args $args)
    {
        $format = $this->registry->get(
            'core/log/format',
            "[ %DATE% %TIME% ] [ %LEVEL% ] [ %SOURCE% ] ~ %MESSAGE%"
        );
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 5);
        $message = str_replace(
            [
                '%DATE%',
                '%TIME%',
                '%LEVEL%',
                '%SOURCE%',
                '%FILE%',
                '%LINE%',
                '%MESSAGE%',
            ],
            [
                date('Y-m-d'),
                date('H:i:s'),
                $this->levelToString[$args->get('level')],
                $args->get('source'),
                $backtrace[4]['file'],
                $backtrace[4]['line'],
                $args->get('message')
            ],
            $format
        );

        return $message;
    }
}
