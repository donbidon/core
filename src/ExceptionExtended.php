<?php
/**
 * Exception class allowing to store and retrive user data.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Core;

/**
 * Exception class allowing to store and retrive user data.
 */
class ExceptionExtended extends \Exception
{
    /**
     * User data
     *
     * @var mixed
     */
    protected $userData;

    /**
     * Sets user data.
     *
     * @param  mixed $data  User data
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Returns user data.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
